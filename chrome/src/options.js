const timezones_div = document.getElementById("timezonesDropdown");

function showTextbox() {
  document.getElementById("timezonesDropdown").classList.toggle("show");
}

const setTimezoneOnClick = (element, index, iana_timezone) => {
  $(element).off('click').on('click', () => {
    chrome.storage.sync.set({options: {timezone: {name: iana_timezone, index}}})
    showTextbox()
    $('#selectedTimezone *').remove()
    showSelectedTimezone(iana_timezone)
  })
}

const showSelectedTimezone = (iana_timezone) => {
  const remove = $('<a href="#"><b>x</b></a>').on('click', () => {
    chrome.storage.sync.set({options: {timezone: undefined}})
    $('#selectedTimezone *').remove()
  })
  const displaySelectedTimezone = $(`<div>${iana_timezone} </div>`)
  displaySelectedTimezone.append(remove)
  $('#selectedTimezone').append(displaySelectedTimezone)
}

chrome.storage.sync.get(null).then(storage => {
  const selectedTimezone = storage?.options?.timezone?.name
  if (selectedTimezone) {
    showSelectedTimezone(selectedTimezone)
  }
})

for (let i = 0; i < timezones.length; i++) {
  const zone = timezones[i]
  const a = $(`<a>${zone.text}</a>`)
  setTimezoneOnClick(a, i, zone.utc[0])
  $(timezones_div).append(a)
  const match_utc = []
  for (const utc of zone.utc) {
    const new_utc = utc.toUpperCase().replace(/[\/_]/g, ' ')
    match_utc.push(new_utc)
  }
  zone.match_utc = match_utc
}
const elements = timezones_div.getElementsByTagName("a");




function filterTimezones() {
  const userInput = document.getElementById("textInput").value.toUpperCase();
  for (let i = 0; i < timezones.length; i++) {
    const e = elements[i]
    const zone = timezones[i]
    const zoneName = zone.text.toUpperCase();
    const zoneUTCMatch = () => {
      for (let j = 0; j < zone.utc.length; j++) {
        if (zone.match_utc[j].indexOf(userInput) > -1) {
          return zone.utc[j]
        }
      }
      return false
    }
    let matched_utc = zoneUTCMatch()
    if (! matched_utc && zoneName.indexOf(userInput) > -1) {
      matched_utc = zone.utc[0]
    }
    if (matched_utc) {
      setTimezoneOnClick(e, i, matched_utc)
      e.style.display = "";
    } else {
      e.style.display = "none";
    }
  }
}




$('.timezones button').click(showTextbox)
$('#textInput').on('keyup', filterTimezones)
