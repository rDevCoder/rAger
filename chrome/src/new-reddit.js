// https://stackoverflow.com/questions/8939467/chrome-extension-to-read-http-response/48134114#48134114

(function(xhr) {

  var XHR = XMLHttpRequest.prototype;

  var open = XHR.open;
  var send = XHR.send;
  var setRequestHeader = XHR.setRequestHeader;

  XHR.open = function(method, url) {
    this._method = method;
    this._url = url;
    this._requestHeaders = {};
    this._startTime = (new Date()).toISOString();

    return open.apply(this, arguments);
  };

  XHR.setRequestHeader = function(header, value) {
    this._requestHeaders[header] = value;
    return setRequestHeader.apply(this, arguments);
  };

  XHR.send = function(postData) {

    this.addEventListener('load', async function() {
      var myUrl = this._url ? this._url.toLowerCase() : this._url;
      // if(myUrl) {
      //
      //   if (postData) {
      //     if (typeof postData === 'string') {
      //       try {
      //         // here you get the REQUEST HEADERS, in JSON format, so you can also use JSON.parse
      //         this._requestHeaders = postData;
      //       } catch(err) {
      //         //console.log('Request Header JSON decode failed, transfer_encoding field could be base64');
      //         //console.log(err);
      //       }
      //     } else if (typeof postData === 'object' || typeof postData === 'array' || typeof postData === 'number' || typeof postData === 'boolean') {
      //         // do something if you need
      //     }
      //   }

        // here you get the RESPONSE HEADERS
        //var responseHeaders = this.getAllResponseHeaders();

        if ( ['','text'].includes(this.responseType) && this.responseText) {
          // responseText is string or null
          try {

            let matcher = this._url.toLowerCase().match(/^https:\/\/(gateway|gql)\.reddit\.com/);
            if (matcher) {
              // here you get RESPONSE TEXT (BODY), in JSON format, so you can use JSON.parse
              var arr = this.responseText;
              var data = JSON.parse(arr);
              let items = {}
              const edges = data.data?.home?.elements?.edges || data.data?.subredditInfoByName?.elements?.edges
              if (matcher[1] === 'gateway') {
                Object.assign(items, data.comments, data.posts)
              } else if (edges.length) {
                // author, authorId, created
                // done: id (fullname)
                for (const e of edges) {
                  if (e?.node?.__typename === 'SubredditPost') {
                    e.node.authorId = e.node.authorInfo?.id
                    e.node.author = e.node.authorInfo?.name
                    e.node.created = Math.floor(Date.parse(e.node.createdAt)/1000)
                    items[e.node.id] = e.node
                  }
                }
              }
              if (Object.keys(items).length) {
                await lookupAuthorDataForItems(items)
                // apply data multiple times b/c content may render slowly
                setTimeout(function () {
                  process_newReddit_items(items);
                  setTimeout(function () {
                    process_newReddit_items(items);
                    setTimeout(function () {
                      process_newReddit_items(items);
                      setTimeout(function () {
                        process_newReddit_items(items);
                      }, 10000);
                    }, 10000);
                  }, 5000);
                }, 1000);
              }
              // printing url, request headers, response headers, response body, to console
              //console.log(this._requestHeaders);
              //console.log(JSON.parse(this._requestHeaders));
              //console.log(responseHeaders);
              //console.log(JSON.parse(arr));
            }

          } catch(err) {
            //console.log("Error in responseType try catch");
            //console.log(err);
          }
        }
    });

    return send.apply(this, arguments);
  };

})(XMLHttpRequest);

// slight delay makes sure all javascript has loaded before trying to call this function
setTimeout(function () {

  // putting the below here instead of src/content.js means it's in same process as the monkey patch.
  // that allows allAuthors to function as a global variable

  process_newReddit(); // processes content that appears in HTML before javascript runs

}, 500);
